data "gitlab_project" "staticsite" {
  path_with_namespace = var.gitlab_project
}

resource "gitlab_project_variable" "aws_access_key_id" {
  project = data.gitlab_project.staticsite.id
  key = "AWS_ACCESS_KEY_ID"
  value = aws_iam_access_key.bucketuser.id
}

resource "gitlab_project_variable" "aws_secret_access_key" {
  project = data.gitlab_project.staticsite.id
  key = "AWS_SECRET_ACCESS_KEY"
  value = aws_iam_access_key.bucketuser.secret
}

resource "gitlab_project_variable" "aws_cloudfront_distribution" {
  project = data.gitlab_project.staticsite.id
  key = "AWS_CLOUDFRONT_ID"
  value = aws_cloudfront_distribution.staticsite.id
}

resource "gitlab_project_variable" "s3_bucket_uri" {
  project = data.gitlab_project.staticsite.id
  key = "S3_BUCKET_URI"
  value = "s3://${aws_s3_bucket.bucket.id}/"
}

resource "gitlab_project_variable" "cloudfront_url" {
  project = data.gitlab_project.staticsite.id
  key = "cloudfrontBaseURL"
  value = "${format("https://%s", "${aws_cloudfront_distribution.staticsite.domain_name}")}"
}

