data "aws_route53_zone" "staticsite" {
  name = var.zone
}

resource "aws_route53_record" "web" {
  type = "A"
  name = var.domain
  zone_id = data.aws_route53_zone.staticsite.zone_id

  alias {
    name = aws_cloudfront_distribution.staticsite.domain_name
    zone_id = aws_cloudfront_distribution.staticsite.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "wwweb" {
  type = "A"
  name = "www.${var.domain}"
  zone_id = data.aws_route53_zone.staticsite.zone_id

  alias {
    name = aws_cloudfront_distribution.staticsite.domain_name
    zone_id = aws_cloudfront_distribution.staticsite.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "cert_validation" {
  count = 2
  zone_id = data.aws_route53_zone.staticsite.zone_id
  type = tolist(aws_acm_certificate.staticsite.domain_validation_options)[count.index].resource_record_type
  name = tolist(aws_acm_certificate.staticsite.domain_validation_options)[count.index].resource_record_name
  records = [
    tolist(aws_acm_certificate.staticsite.domain_validation_options)[count.index].resource_record_value
  ]
  ttl = 60
  allow_overwrite = true
}
