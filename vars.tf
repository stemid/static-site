variable "zone" {
  type = string
}

variable "domain" {
  type = string
}

variable "gitlab_project" {
  type = string
}
