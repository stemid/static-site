data "archive_file" "redirect_zip" {
  type = "zip"
  source_file = "${path.module}/redirect.js"
  output_path = "${path.module}/redirect.zip"
}

data "aws_iam_policy_document" "lambda_assume" {
  statement {
    effect = "Allow"

    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
        "edgelambda.amazonaws.com"
      ]
    }

    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "lambda_logging" {
  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]

    resources = ["arn:aws:logs:*:*:*"]
  }
}

resource "aws_iam_role" "lambda" {
  name_prefix = "${var.domain}-"
  assume_role_policy = data.aws_iam_policy_document.lambda_assume.json
  provider = aws.us-east-1
}

resource "aws_iam_role_policy" "lambda_logging" {
  name_prefix = "${var.domain}-"
  role = aws_iam_role.lambda.id
  policy = data.aws_iam_policy_document.lambda_logging.json
  provider = aws.us-east-1
}

resource "random_string" "random" {
  length = 16
  upper = false
  special = false
}

resource "aws_lambda_function" "subfolder_index_redirect" {
  filename = "${path.module}/redirect.zip"
  function_name = "subfolder-index-redirect-${random_string.random.id}"
  handler = "redirect.handler"
  source_code_hash = data.archive_file.redirect_zip.output_base64sha256
  publish = true
  role = aws_iam_role.lambda.arn
  runtime = "nodejs18.x"
  provider = aws.us-east-1
}

resource "aws_s3_bucket" "bucket" {
  bucket = "${var.domain}-${random_string.random.id}"
}

data "aws_iam_policy_document" "bucket_policy" {
  statement {
    principals {
      type = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.staticsite.iam_arn]
    }

    actions = [
      "s3:GetObject",
      "s3:ListBucket",
    ]

    resources = [
      aws_s3_bucket.bucket.arn,
      "${aws_s3_bucket.bucket.arn}/*"
    ]
  }
}

resource "aws_s3_bucket_policy" "cloudfront_policy" {
  bucket = aws_s3_bucket.bucket.id
  policy = data.aws_iam_policy_document.bucket_policy.json
}

# This bucket user is for pipelines that upload files into the S3 bucket
resource "aws_iam_user" "bucketuser" {
  name = "${var.domain}-${random_string.random.id}"
}

resource "aws_iam_access_key" "bucketuser" {
  user = aws_iam_user.bucketuser.name
}

data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "userpolicy" {
  statement {
    effect = "Allow"
    actions = [
      "s3:ListBucket",
    ]
    resources = [
      "${aws_s3_bucket.bucket.arn}",
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "s3:DeleteObject",
      "s3:PutObject",
    ]
    resources = [
      "${aws_s3_bucket.bucket.arn}/*",
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "cloudfront:CreateInvalidation"
    ]
    resources = [
      "arn:aws:cloudfront::${data.aws_caller_identity.current.account_id}:distribution/${aws_cloudfront_distribution.staticsite.id}"
    ]
  }
}

resource "aws_iam_user_policy" "userpolicy" {
  name = "userpolicy"
  user = aws_iam_user.bucketuser.name
  policy = data.aws_iam_policy_document.userpolicy.json
}

resource "aws_acm_certificate" "staticsite" {
  domain_name = var.domain
  subject_alternative_names = ["www.${var.domain}"]
  validation_method = "DNS"
  provider = aws.us-east-1
}

resource "aws_acm_certificate_validation" "staticsite" {
  certificate_arn = aws_acm_certificate.staticsite.arn
  validation_record_fqdns = [for record in aws_route53_record.cert_validation : record.fqdn]
  provider = aws.us-east-1
}

resource "aws_cloudfront_origin_access_identity" "staticsite" {}

resource "aws_cloudfront_distribution" "staticsite" {
  depends_on = [aws_acm_certificate_validation.staticsite]

  origin {
    domain_name = "${aws_s3_bucket.bucket.bucket_regional_domain_name}"
    origin_id   = "S3-${aws_s3_bucket.bucket.id}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.staticsite.cloudfront_access_identity_path
    }
  }

  enabled = true
  is_ipv6_enabled = true
  default_root_object = "index.html"
  aliases = [var.domain, "www.${var.domain}"]

  default_cache_behavior {
    allowed_methods = ["GET", "HEAD"]
    cached_methods = ["GET", "HEAD"]
    target_origin_id = "S3-${aws_s3_bucket.bucket.id}"
    viewer_protocol_policy = "redirect-to-https"
    min_ttl = 0
    default_ttl = 3600
    max_ttl = 86400

    forwarded_values {
      query_string = true

      cookies {
        forward = "all"
      }
    }

    lambda_function_association {
      event_type = "origin-request"
      lambda_arn = aws_lambda_function.subfolder_index_redirect.qualified_arn
      include_body = false
    }
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.staticsite.arn
    ssl_support_method = "sni-only"
  }

  restrictions {
    geo_restriction {
      # Set to "none" and remove locations if you want no resitrctions
      restriction_type = "none"
      locations = []
    }
  }

  custom_error_response {
    error_code = 404
    response_code = 404
    response_page_path = "/404.html"
  }
}

