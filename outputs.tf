output "url" {
  value = "${format("https://%s", "${aws_cloudfront_distribution.staticsite.domain_name}")}"
}

output "aws_access_key_id" {
  value = aws_iam_access_key.bucketuser.id
}

output "aws_secret_access_key" {
  value = aws_iam_access_key.bucketuser.secret
  sensitive = true
}

output "cloudfront_distribution_id" {
  value = aws_cloudfront_distribution.staticsite.id
}

output "name_servers" {
  value = data.aws_route53_zone.staticsite.name_servers
}
