# Static site in AWS

Terraform module I use to setup static websites in AWS, while hosting and deploying them from Gitlab.

* Hosted zone must already exist, this module does not attempt to create zone.
* Creates TLS certificate in ACM.
* Creates S3 bucket for storing website.
* Creates IAM user and policy to access S3 bucket.
* Creates Cloudfront distribution to front S3 bucket.
* Creates A pointer in Route53 for ``${var.domain}`` and ``www.${var.domain}``.
* Creates Gitlab project variables with AWS access key to upload website into S3 from CD pipeline.

# Example

```
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 5.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "16.6.0"
    }
  }
  required_version = ">= 1.5.1"
}

provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_access_key
  region  = var.aws_default_region

  default_tags {
    tags = {
      ProjectType = "static-site"
    }
  }
}

# Alias the aws provider to use with lambda@Edge which must be in us-east-1
provider "aws" {
  alias = "us-east-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_access_key
  region  = "us-east-1"

  default_tags {
    tags = {
      ProjectType = "static-site"
    }
  }
}

provider "gitlab" {
  token = var.gitlab_token
}

module "static-site" {
  source = "git::https://gitlab.com/stemid/static-site.git"
  zone = "mydomain.tld"
  # Domain can be a subdomain of zone.
  domain = "mydomain.tld"
  gitlab_project = "stemid/mydomain.tld"
  providers = {
    aws = aws
    aws.us-east-1 = aws.us-east-1
  }
}
```
